
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Home.vue') },
      { path: 'catalog', component: () => import('pages/Catalog.vue') },
      { path: 'about', component: () => import('pages/AboutCompany.vue') },
      { path: 'info', component: () => import('pages/DeliveryPay.vue') },
      { path: 'basket', component: () => import('pages/Basket.vue') },
      { path: 'bouquet',
        component: () => import('pages/PageProduct.vue'),
        children: [
          {
            name: 'bouquet.page',
            path: '/bouquet/:slug',
            component: () => import('pages/PageProduct.vue')
          }
        ]
      },
      { path: 'cabinet', component: () => import('pages/LCabinet.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
