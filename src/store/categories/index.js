import api from '../../boot/api'
const state = {
  categories: null
}

const getters = {
  categories: state => state.categories,
  defCategory: state => type => {
    return state.categories.filter(el => el.type === type)
  }
}

const mutations = {
  GET_CATEGORIES (state, data) {
    state.categories = data
  }
}

const actions = {
  getCategories (context) {
    return new Promise((resolve, reject) => {
      api.get('categories')
        .then(response => {
          context.commit('GET_CATEGORIES', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
