import api from '../../boot/api'
const state = {
  count: 0,
  products: null,
  popularProducts: null,
  filterProducts: null
}

const getters = {
  products: state => state.products,
  popularProducts: state => state.popularProducts,
  filterProducts: state => state.filterProducts
}

const actions = {
  getProducts (context) {
    return new Promise((resolve, reject) => {
      api.get('products')
        .then(response => {
          context.commit('GET_PRODUCTS', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  getPopular (context) {
    return new Promise((resolve, reject) => {
      api.get('products/popular')
        .then(response => {
          context.commit('GET_POPULAR', response.data.response)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  filterProducts (context, data) {
    return new Promise((resolve, reject) => {
      api.post('products/filter', data)
        .then(response => {
          context.commit('GET_FILTER', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  increasePrice (context) {
    context.commit('INCREASE')
  },
  decreasePrice (context) {
    context.commit('DECREASE')
  }
}

const mutations = {
  GET_PRODUCTS (state, data) {
    state.products = data
  },
  GET_POPULAR (state, data) {
    state.popularProducts = data
  },
  GET_FILTER (state, data) {
    state.filterProducts = data
  },
  INCREASE (state) {
    state.count += 1
  },
  DECREASE (state) {
    state.count -= 1
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
