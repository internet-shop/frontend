import Vue from 'vue'
import Vuex from 'vuex'
import products from './products'
import auth from './auth'
import user from './auth/user'
import categories from './categories'
import basket from './basket'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      products, auth, categories, user, basket
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
