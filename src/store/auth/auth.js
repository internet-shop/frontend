import API_URL from '../../boot/api'

const state = {
  token: localStorage.getItem('user-token') || ''
}

const getters = {
  isAuth: (state) => {
    if (state.token === '') {
      return false
    } else {
      return true
    }
  }
}

const mutations = {
  AUTH_SUCCESS (state, data) {
    state.token = data
  },
  AUTH_LOGOUT (state) {
    state.token = ''
  }
}

const actions = {
  register ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API_URL.post('auth/register', data)
        .then((response) => {
          const token = response.data.token
          commit('AUTH_SUCCESS', token)
          localStorage.setItem('user-token', token)
          API_URL.defaults.headers.common['Authorization'] = 'Bearer ' + token
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  login ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API_URL.post('auth/login', data)
        .then((response) => {
          const token = response.data.token
          commit('AUTH_SUCCESS', token)
          localStorage.setItem('user-token', token)
          API_URL.defaults.headers.common['Authorization'] = 'Bearer ' + token
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  logout ({ commit }) {
    return new Promise((resolve, reject) => {
      API_URL.get('auth/logout')
        .then((response) => {
          commit('AUTH_LOGOUT')
          localStorage.removeItem('user-token')
          delete API_URL.defaults.headers.common['Authorization']
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateAuth () {
    const token = localStorage.getItem('user-token')
    if (token) {
      API_URL.defaults.headers.common['Authorization'] = 'Bearer ' + token
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
