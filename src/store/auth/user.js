import api from '../../boot/api'

const state = {
  user: null,
  orders: 0,
  history: null
}

const getters = {
  user: state => state.user,
  orders: state => {
    if (this.user === null) {
      return state.orders
    } else {
      return state.user.orders
    }
  },
  history: state => state.history
}

const mutations = {
  GET_USER (state, data) {
    state.user = data
    state.url += data.id
  },
  GET_HISTORY (state, data) {
    state.history = data
  }
}

const actions = {
  getUser ({ commit }) {
    return new Promise((resolve, reject) => {
      api.get('users/authuser')
        .then((response) => {
          commit('GET_USER', response.data.user)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateInfo ({ commit }, data) {
    return new Promise((resolve, reject) => {
      api.put('users/' + data.id, data.data)
        .then((response) => {
          commit('GET_USER', response.data.user)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getHistory ({ commit }) {
    return new Promise((resolve, reject) => {
      api.get('users/history')
        .then((response) => {
          commit('GET_HISTORY', response.data.data)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
