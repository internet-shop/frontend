import api from '../../boot/api'
const state = {
  basket: []
}

const getters = {
  basket: state => state.basket
}

const mutations = {
  ADD_PRODUCT (state, data) {
    state.basket.push(data)
  }
}

const actions = {
  addProduct ({ commit }, data) {
    return new Promise((resolve, reject) => {
      api.post('products/' + data.id + '/tocart', data.count)
        .then((response) => {
          commit('ADD_PRODUCT', data)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
